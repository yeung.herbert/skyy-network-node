package rest

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"github.com/cosmos/cosmos-sdk/client/context"
	"github.com/cosmos/cosmos-sdk/types/rest"
)

type ping struct {
	message   string
	timestamp string
	data      string
	signature string
}

// RegisterRoutes registers network_node-related REST handlers to a router
func RegisterRoutes(cliCtx context.CLIContext, r *mux.Router, storeName string) {
	// registerQueryRoutes(cliCtx, r)
	// registerTxRoutes(cliCtx, r)

	// Health check endpoint
	r.HandleFunc(fmt.Sprintf("/%s/ping", storeName), pingHandler(clicCtx, storeName))
}

// CORS support
func enableCors(w *http.ResponseWriter, r *http.Request) {
	origin := r.Header.Get("Origin")
	(*w).Header().Set("Access-Control-Allow-Origin", origin)
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
	(*w).Header().Set("Access-Control-Allow-Headers", "Authorization,Accept,Origin,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range")
	(*w).Header().Set("Access-Control-Allow-Methods ", "GET,POST,OPTIONS,PUT,DELETE,PATCH")
}

// Handlers
func pingHandler(cliCtx context.CLIContext, storeName string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w, r)
		sig := sha256.New()
		pingResp := "pong"
		sig.Write([]byte(pingResp))
		pingRespMsg := ping{
			message:   pingResp,
			timestamp: time.Now().String(),
			data:      "", // TODO: Does this have to be null? Have to use pointers?
			signature: base64.URLEncoding.EncodeToString(sig.Sum(nil)),
		}
		var jsonData []byte
		jsonData, err := json.Marshal(pingRespMsg)
		if err != nil {
			rest.WriteErrorResponse(w, http.StatusBadRequest, "marshal json err")
			return
		}
		fmt.Fprintf(w, string(jsonData))
	}
}
