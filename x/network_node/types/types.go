package types

import (
	"fmt"
	"strings"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

// MinNamePrice is blah blah
var MinNamePrice = sdk.Coins{sdk.NewInt64Coin("nameToken", 1)}

// NetworkNode is blah blah
type NetworkNode struct {
	Value string         `json:"`
	Owner sdk.AccAddress `json:"owner"`
	Price sdk.Coins      `json:"price"`
}

// NewNetworkNode returns blah blah
func NewNetworkNode() NetworkNode {
	return NetworkNode{
		Price: MinNamePrice,
	}
}

func (n NetworkNode) String() string {
	return strings.TrimSpace(fmt.Sprintf(`Owner: %s
	Value %s
	Price %s`, n.Owner, n.Value, n.Price))
}
